﻿/*
Zaprojektować, zaimplementować oraz przetestować klasę opisującą punkt w przestrzeni trójwymiarowej.
Klasa powinna dostarczać następującą funkcjonalność:
· indeksowany dostęp do współrzędnej punktu,
· wyznaczanie odległości między dwoma punktami,
· dodawanie/odejmowanie dwóch punktów - realizowane jako suma/różnica odpowiadających sobie współrzędnych punktów,
· symetryczny iloczyn punktu z liczbą zmiennoprzecinkową - realizowany jako iloczyn współrzędnych punktu i liczby,
· porównanie dwóch punktów na równość i mniejszość; punkty są sobie równe jeśli ich odległość od
	początku układu współrzędnych jest taka sama (z dokładnością 10-10); punkt jest mniejszy od innego
	jeżeli leży bliżej początku układu współrzędnych,
· dostęp do strumienia (wejściowego i wyjściowego).
*/
#include "pch.h"
#include <iostream>
#include <math.h>
using namespace std;
const double eps = 1e-10;    
class point
{
	double tablica[3];

public:
	point()
	{
		tablica[0] = 0;
		tablica[1] = 0;
		tablica[2] = 0;
	}

	point(const double(&r)[3])       
	{
		tablica[0] = r[0];
		tablica[1] = r[1];
		tablica[2] = r[2];
	}

	point(const double &a, const double &b, const double &c)     
	{
		tablica[0] = a;
		tablica[1] = b;
		tablica[2] = c;
	}

	point(const point & copy)
	{
		tablica[0] = copy.tablica[0];
		tablica[1] = copy.tablica[1];
		tablica[2] = copy.tablica[2];
	}

	const double & operator [] (size_t i) const       
	{
		if (i > 2) { cout << "Out of range" << endl; }
		return tablica[i];
	}

	const point operator * (double n) const 
	{
		return point(this->tablica[0] * n, tablica[1] * n, tablica[2] * n);
	}
	
	double distance(const point & B) const
	{
		return (sqrt((B[0] - tablica[0])*(B[0] - tablica[0]) + (B[1] - tablica[1])*(B[1] - tablica[1]) + (B[2] - tablica[2])*(B[2] - tablica[2])));
	}

	bool operator < (const point& T) const  
	{
		if (tablica[0] < T[0] && tablica[1] < T[1] && tablica[2] < T[2]) { return true; }
		else { return false; }
	}

	bool operator == (const point & T) const  
	{
		if (tablica[0] == T[0] && tablica[1] == T[1] && tablica[2] == T[2]) { return true; }
		else { return false; }
	}

	friend ostream & operator << (ostream & out, const point &r);     
	//friend istream & operator >> (istream & in, point &r);            
};


const point operator * (double n, point A) 
{
	return point(n* A[0], n* A[1], n * A[2]);
}
const point operator + (const point & A_0, const point & A)        
{
	return point(A_0[0] + A[0], A_0[1] + A[1], A_0[2] + A[2]);
}
const point operator - (const point & B_0, const point & B) 
{
	return point(B_0[0] - B[0], B_0[1] - B[1], B_0[2] - B[2]);
}

ostream & operator << (ostream & out, const point & r)
{
	return out << "(" << r[0] << ")" << "(" << r[1] << ")" << "(" << r[2] << ")";
}
istream & operator >> (istream & in, const point &r)
{
	return in >> point(r[0], r[1], r[2]);
}

int main()
{
	double x[2][3] = { {1.0, 1.0, 1.0}, {1.0, 2.0, 3.0} };      
	point p1(x[0]), p2(x[1]);      
	const point p3(0.4, 0.2, 0.1);

	cout << p1 << ", " << endl << p2 << '\n';
	cout << p3[0] << ' ' << p3[1] << ' ' << p3[2] << '\n';
	cout << p3 << ", " << p2 << '\n';
	cout << p1.distance(point()) << ", " << p3.distance(p1) << '\n';

	cout << p1 + p2 << " , " << p1 - p3 << '\n';
	cout << 3.14 * p2 << ", = " << p2 * 3.14 << '\n';
	cout << (p1 < p3) << " (FALSZ), " << (p1 == point(1.0, 1.0, 1.0)) << " (PRAWDA) \n";

	//cin >> p1;       
	//cout << p1 << '\n';

	//system("Pause");
	return 0;
}